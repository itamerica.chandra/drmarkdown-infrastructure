
#!/bin/bash

kubectl apply -f secrets/registry-credentials.yaml

kubectl apply -f deployments/auth-deployment.yaml
kubectl apply -f deployments/doc-deployment.yaml
kubectl apply -f deployments/frontend-deployment.yaml

kubectl apply -f configmaps/nginx-ingress-controller-config.yaml


kubectl apply -f roles/nginx-ingress-controller-account.yaml

kubectl apply -f services/auth-service.yaml
kubectl apply -f services/doc-service.yaml
kubectl apply -f services/frontend-service.yaml

kubectl apply -f ingresses/auth-ingress.yaml
kubectl apply -f ingresses/doc-ingress.yaml
kubectl apply -f ingresses/frontend-ingress.yaml

kubectl apply -f deployments/nginx-ingress-controller.yaml

kubectl apply -f public-ingress.yaml
